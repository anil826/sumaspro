require 'csv'

puts "Importing courses..."
CSV.foreach(Rails.root.join("courses.csv"), headers: true) do |row|
  Course.create! do |course|
    course.id = row[0]
    course.name = row[1]
  end
end

puts "Importing Paper..."
CSV.foreach(Rails.root.join("papers.csv"), headers: true) do |row|
  Paper.create! do |paper|
    paper.name = row[1]
    paper.course_id = row[2]
  end
end

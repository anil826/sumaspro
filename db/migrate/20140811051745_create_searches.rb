class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.integer :course_id
      t.integer :paper_id

      t.timestamps
    end
  end
end

class CreateQsearches < ActiveRecord::Migration
  def change
    create_table :qsearches do |t|
      t.integer :course_id
      t.integer :paper_id

      t.timestamps
    end
  end
end

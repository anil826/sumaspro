class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.integer :course_id
      t.integer :paper_id

      t.timestamps
    end
  end
end

# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $('#download_paper_id').parent().hide()
  papers = $('#download_paper_id').html()
  $('#download_course_id').change ->
    courses = $('#download_course_id :selected').text()
    escaped_courses = courses.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(papers).filter("optgroup[label='#{escaped_courses}']").html()
    if options
      $('#download_paper_id').html(options)
      $('#download_paper_id').parent().show()
    else
      $('#download_paper_id').empty()
      $('#download_paper_id').parent().hide()

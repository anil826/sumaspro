# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
  $('#qsearch_paper_id').parent().hide()
  papers = $('#qsearch_paper_id').html()
  $('#qsearch_course_id').change ->
    courses = $('#qsearch_course_id :selected').text()
    escaped_courses = courses.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(papers).filter("optgroup[label='#{escaped_courses}']").html()
    if options
      $('#qsearch_paper_id').html(options)
      $('#qsearch_paper_id').parent().show()
    else
      $('#qsearch_paper_id').empty()
      $('#qsearch_paper_id').parent().hide()

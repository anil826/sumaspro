# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $('#search_paper_id').parent().hide()
  papers = $('#search_paper_id').html()
  $('#search_course_id').change ->
    courses = $('#search_course_id :selected').text()
    escaped_courses = courses.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(papers).filter("optgroup[label='#{escaped_courses}']").html()
    if options
      $('#search_paper_id').html(options)
      $('#search_paper_id').parent().show()
    else
      $('#search_paper_id').empty()
      $('#search_paper_id').parent().hide()

ActiveAdmin.register Download do
  index do

     column "Paper" do |m|
			 paper = Paper.find(m.paper_id).name
		 end
		 column "Course" do |m|
				 course = Course.find(m.course_id).name
		 end

    column :attachment
    column :name
    end

end

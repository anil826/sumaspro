class QuestionPaperUploader < CarrierWave::Uploader::Base

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    %w(pdf doc htm html docx xls pptx rar)
  end

	def filename
	@name ||= "#{File.basename(original_filename, '.*')}" if original_filename.present?
		end

end

class Search < ActiveRecord::Base


	def downloads
			@downloads ||= find_downloads
	end

	private
		def find_downloads
		  downloads = Download.order(:attachment)
		  downloads = downloads.where(paper_id: paper_id) if paper_id.present?
		  downloads
		end

end

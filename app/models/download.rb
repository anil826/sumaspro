class Download < ActiveRecord::Base
  mount_uploader :attachment, AttachmentUploader
    validates :course_id, presence: true
    validates :paper_id, presence: true
    validates :attachment, presence: true
end

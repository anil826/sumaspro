class Qsearch < ActiveRecord::Base

 def questions
			@questions ||= find_questions
	end

	private
		def find_questions
		  questions = Question.order(:question_paper)
		  questions = questions.where(paper_id: paper_id) if paper_id.present?
		  questions
		end
end

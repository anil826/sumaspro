class Question < ActiveRecord::Base
  mount_uploader :question_paper, QuestionPaperUploader
    validates :course_id, presence: true
    validates :paper_id, presence: true
    validates :question_paper, presence: true
#    has_many :course
#    has_many :paper
end

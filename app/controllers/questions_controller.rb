class QuestionsController < ApplicationController

	def index
       @questions = Question.search_question(params[:qsearch])
		end


  def new
		@question = Question.new
  end

  def create
  @question = Question.new(question_params)
    if @question.save
      redirect_to admin_questions_path, notice: "The question #{@question.name} has been uploaded."
	    else
	      render "new"
	    end
  end

 def destroy
    @question = question.find(params[:id])
    @question.destroy
    redirect_to questions_path, notice:  "The question #{@question.name} has been deleted."
  end

 private
  def question_params
params.require(:question).permit(:name,:course_id, :question_paper, :paper_id)
  end

end

class QsearchesController < InheritedResources::Base

  def new
	 @qsearch = Qsearch.new
	end

	 def create
			@qsearch = Qsearch.create!(qsearch_params)
			redirect_to @qsearch
	 end

  def show
		@qsearch = Qsearch.find(params[:id])
	end

 private
  def qsearch_params
    params.require(:qsearch).permit(:name,:course_id, :question_paper, :paper_id)
  end

end

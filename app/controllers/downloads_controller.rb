class DownloadsController < ApplicationController
  def index
    @downloads = Download.search(params[:search])

  end

  def new
    @download = Download.new
  end

  def create

    @download = Download.new(download_params)
    if @download.save
      redirect_to admin_downloads_path, notice: "The download #{@download.name} has been uploaded."
	    else
	      render "new"
	    end
  end

  def destroy
    @download = download.find(params[:id])
    @download.destroy
    redirect_to downloads_path, notice:  "The download #{@download.name} has been deleted."
  end

  private
  def download_params
    params.require(:download).permit(:name,:course_id, :attachment, :paper_id)
  end

end

class ContactsController < ApplicationController

 #before_action :default_action_tab
  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        ContactMailer.contact(@contact).deliver!
        format.html { redirect_to root_path, notice: 'Your Contact info has been sent successfully.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

 private

    def contact_params
      params.require(:contact).permit(:name,:email, :message,:number)
    end

		def default_action_tab
	  	@action = "contact"
		end

    end

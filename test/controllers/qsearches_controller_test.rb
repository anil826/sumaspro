require 'test_helper'

class QsearchesControllerTest < ActionController::TestCase
  setup do
    @qsearch = qsearches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:qsearches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create qsearch" do
    assert_difference('Qsearch.count') do
      post :create, qsearch: { course_id: @qsearch.course_id, paper_id: @qsearch.paper_id }
    end

    assert_redirected_to qsearch_path(assigns(:qsearch))
  end

  test "should show qsearch" do
    get :show, id: @qsearch
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @qsearch
    assert_response :success
  end

  test "should update qsearch" do
    patch :update, id: @qsearch, qsearch: { course_id: @qsearch.course_id, paper_id: @qsearch.paper_id }
    assert_redirected_to qsearch_path(assigns(:qsearch))
  end

  test "should destroy qsearch" do
    assert_difference('Qsearch.count', -1) do
      delete :destroy, id: @qsearch
    end

    assert_redirected_to qsearches_path
  end
end

set :branch, "master"

set :domain, '10.0.0.41'

set :user, 'ubuntu'

set :unicorn_worker_count, 4

set :ssl_enabled, false

set :web_ports, [10016, 10017]
#set :web_ports, [10016]

set :worker_ports, [10019]

task :set_sudo_password => :environment do
  queue! "echo '#{erb(File.join(__dir__,'sudo_password.erb'))}' > /home/ubuntu/SudoPass.sh"
  queue! "chmod +x /home/ubuntu/SudoPass.sh"
  queue! "export SUDO_ASKPASS=/home/ubuntu/SudoPass.sh"
end

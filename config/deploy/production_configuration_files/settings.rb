set :branch, 'master'

set :domain, '54.187.232.224'

set :user, 'ubuntu'

set :unicorn_worker_count, 4

set :ssl_enabled, false

task :set_sudo_password => :environment do
  queue! "echo '#{erb(File.join(__dir__,'sudo_password.erb'))}' > /home/ubuntu/SudoPass.sh"
  queue! "chmod +x /home/ubuntu/SudoPass.sh"
  queue! "export SUDO_ASKPASS=/home/ubuntu/SudoPass.sh"
end

Rails.application.routes.draw do


  resources :qsearches

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users do
         get '/users/sign_out' => 'devise/sessions#destroy'
         match 'register' => 'users#create', :as => :register
  end


  resources :contacts
   root 'home#index'
   get '/about', :to => "home#about", as: :about
   get 'blog/index'
   get 'course/index'
   get 'course/show'
  resources :downloads,  only: [:index, :new, :create, :destroy]
  resources :questions,  only: [:index, :new, :create, :destroy]
  resources :searches
  resources :results
end
